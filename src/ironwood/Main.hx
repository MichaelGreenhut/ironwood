package ironwood;

import flambe.display.Sprite;
import urgame.Console;
import urgame.StoryBuilder;
import flambe.Entity;
import flambe.System;
import flambe.asset.AssetPack;
import flambe.asset.Manifest;
import flambe.display.FillSprite;
import flambe.display.ImageSprite;

class Main
{
	private static var _packNames:Array<String>;
	private static var _console:Console;
	private static var _container:Sprite;
	private static var _storyBuilder:StoryBuilder;
	
    private static function main ()
    {
        // Wind up all platform-specific stuff
        System.init();
		_packNames = ["bootstrap","two"];
		_storyBuilder = new StoryBuilder(_packNames, "storymap.xml");
	}
}
